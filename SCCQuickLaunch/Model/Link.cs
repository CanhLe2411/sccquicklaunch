﻿using SCCQuickLaunch.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCCQuickLaunch.Model
{
    public partial class Link: BaseViewModel
    {
        public Link()
        {
            Objects = new HashSet<Object>();
        }
        public string ABC = "TEST 123";
        private string _Url;
        public string Url
        {
            get => _Url;
            set
            {
                _Url = value;
                OnPropertyChanged();
            }
        }

        public virtual ICollection<Object> Objects { get; set; }
    }
}
