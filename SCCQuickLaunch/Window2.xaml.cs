﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SCCQuickLaunch
{
    /// <summary>
    /// Interaction logic for Window2.xaml
    /// </summary>
    public partial class Window2 : Window
    {
        public Window2()
        {
            InitializeComponent();

            var height = SystemParameters.WorkArea.Height;
            if (height < 1050) // width = 450
            {
                var percent = height / 1050;
                Height = Height * percent;
                Width = Width * percent;
            }

            // Position
            Left = SystemParameters.WorkArea.Width - Width;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            WindowStyle = WindowStyle.None;
        }
    }
}
