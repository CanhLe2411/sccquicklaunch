﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SCCQuickLaunch.customcontrols
{
    /// <summary>
    /// Interaction logic for StarCard.xaml
    /// </summary>
    public partial class StarCard : UserControl
    {
        public StarCard()
        {
            InitializeComponent();

            SetScale();
        }
        public string Source
        {
            get { return (string)GetValue(SrcProperty); }
            set { SetValue(SrcProperty, value); }
        }

        public static readonly DependencyProperty SrcProperty =
            DependencyProperty.Register("Source", typeof(string), typeof(StarCard));

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("Title", typeof(string), typeof(StarCard));

        private void SetScale()
        {
            var percent = SharedFunc.GetPercent();
            bdrImage.CornerRadius = new CornerRadius
            {
                BottomLeft = bdrImage.CornerRadius.BottomLeft * percent,
                BottomRight = bdrImage.CornerRadius.BottomRight * percent,
                TopLeft = bdrImage.CornerRadius.TopLeft * percent,
                TopRight = bdrImage.CornerRadius.TopRight * percent
            };
            bdrImage.Width *= percent;
            bdrImage.Height *= percent;
            bdrImage.MinWidth *= percent;
            bdrImage.MinHeight *= percent;

            imgImage.Width *= percent;
            imgImage.Height *= percent;
        }
    }
}
