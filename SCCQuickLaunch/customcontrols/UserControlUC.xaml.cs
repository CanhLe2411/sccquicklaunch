﻿using SCCQuickLaunch.ViewModel;
using System.Windows.Controls;

namespace SCCQuickLaunch.customcontrols
{
    /// <summary>
    /// Interaction logic for UserControlUC.xaml
    /// </summary>
    public partial class UserControlUC : UserControl
    {
        public ControlBarViewModel Viewmodel { get; set; }

        public UserControlUC()
        {
            InitializeComponent();
            this.DataContext = Viewmodel = new ControlBarViewModel();

            SetScale();
        }

        private void SetScale()
        {
            var percent = SharedFunc.GetPercent();

            btnSetting.Width *= percent;
            icoSetting.Width *= percent;
            icoSetting.Height *= percent;
            btnSetting.Margin = new System.Windows.Thickness
            {
                Right = btnReload.Margin.Right * percent
            };

            btnClose.Width *= percent;
            icoClose.Width *= percent;
            icoClose.Height *= percent;
            btnClose.Margin = new System.Windows.Thickness
            {
                Right = btnReload.Margin.Right * percent
            };

            btnReload.Width *= percent;
            icoReload.Width *= percent;
            icoReload.Height *= percent;
            btnReload.Margin = new System.Windows.Thickness { 
                Right = btnReload.Margin.Right * percent
            };
        }
    }
}
