﻿using SCCQuickLaunch.Common;
using SCCQuickLaunch.Model;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Windows;

namespace SCCQuickLaunch
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        private string _passPhrase = "3226c812e0c790e80839443e291f8ad4c53c53e6";
        private bool _checking = false;

        public Settings(bool checking = false)
        {
            InitializeComponent();
            _checking = checking;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var proxyAccount = ReadXML();
            if (proxyAccount != null)
            {
                txtUserName.Text = proxyAccount.UserName;
                txtPassword.Password = proxyAccount.Password;
                if (_checking)
                {
                    if (!string.IsNullOrWhiteSpace(proxyAccount.UserName)
                        && !string.IsNullOrWhiteSpace(proxyAccount.Password))
                    {
                        ConnectProxy().GetAwaiter();
                    }
                    else
                    {
                        Close();
                    }
                }
            }
            else if (_checking)
            {
                Close();
            }
            txtUserName.Focus();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            var userName = txtUserName.Text;
            var password = txtPassword.Password;

            if(!string.IsNullOrWhiteSpace(userName) && !string.IsNullOrWhiteSpace(password))
            {
                WriteXML(userName, password);
                MessageBox.Show("Save successfully!");
                this.Close();
            }
            else
            {
                MessageBox.Show("Please input your UserName and Password!");
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtUserName.Text = string.Empty;
            txtPassword.Password = string.Empty;
            WriteXML(string.Empty, string.Empty);
            txtUserName.Focus();
        }

        private void WriteXML(string userName, string password)
        {
            var proxyAccount = new ProxyAccount { 
                UserName = userName,
                Password = StringCipher.Encrypt(password, _passPhrase)
            };

            XmlClassHelper.WriteXML(proxyAccount, "ProxyAccount.xml");
        }

        private ProxyAccount ReadXML()
        {
            var encodeInfo = XmlClassHelper.ReadXML<ProxyAccount>("ProxyAccount.xml");
            if(encodeInfo != null && !string.IsNullOrWhiteSpace(encodeInfo.Password))
            {
                encodeInfo.Password = StringCipher.Decrypt(encodeInfo.Password, _passPhrase);
            }
            return encodeInfo;
        }

        private async void btnConnect_Click(object sender, RoutedEventArgs e)
        {
            await ConnectProxy();
        }

        private async Task ConnectProxy()
        {
            EnableFields(false);
            btnConnect.Content = "Connecting...";

            if(!IsConnectedToInternet() || !_checking)
            {
                // POST http://proxy.scancom.net/index.php
                // Content-Type: application/x-www-form-urlencoded
                // username=ho.van.tinh&password=test
                string url = "http://proxy.scancom.net/index.php";
                var client = new HttpClient();
                var dict = new Dictionary<string, string>() {
                    { "username", txtUserName.Text },
                    { "password", txtPassword.Password }
                };
                var res = await client.PostAsync(url, new FormUrlEncodedContent(dict));
                var content = await res.Content.ReadAsStringAsync();
                if(content.Contains("CONGRATULATION !!!!"))
                {
                    MessageBox.Show("Connected proxy successfully!", "Proxy Auto");
                }
            }

            if(_checking)
            {
                Close();
            }

            btnConnect.Content = "Connect";
            EnableFields(true);
        }

        private void EnableFields(bool enabled)
        {
            txtUserName.IsEnabled = enabled;
            txtPassword.IsEnabled = enabled;
            btnSave.IsEnabled = enabled;
            btnClear.IsEnabled = enabled;
            btnConnect.IsEnabled = enabled;
        }

        public bool IsConnectedToInternet()
        {
            string url = "http://www.google.com";
            try
            {
                System.Net.WebRequest myRequest = System.Net.WebRequest.Create(url);
                System.Net.WebResponse myResponse = myRequest.GetResponse();
                return !myResponse.ResponseUri.Host.Contains("proxy.scancom.net");
            }
            catch (System.Net.WebException)
            {
                return false;
            }
        }
    }
}
