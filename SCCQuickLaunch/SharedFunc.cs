﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SCCQuickLaunch
{
    public static class SharedFunc
    {
        public static double GetPercent()
        {
            var defaultHeight = 1050;
            var height = SystemParameters.WorkArea.Height;
            if (height != defaultHeight) // width = 410
            {
                return height / defaultHeight;
            }
            return 1;
        }

        public static bool DatesAreInTheSameWeek(DateTime date1, DateTime date2)
        {
            var cal = System.Globalization.DateTimeFormatInfo.CurrentInfo.Calendar;
            var d1 = date1.Date.AddDays(-1 * (int)cal.GetDayOfWeek(date1));
            var d2 = date2.Date.AddDays(-1 * (int)cal.GetDayOfWeek(date2));

            return d1 == d2;
        }
    }
}
