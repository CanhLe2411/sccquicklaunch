﻿using Newtonsoft.Json;
using SCCQuickLaunch.customcontrols;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace SCCQuickLaunch
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        protected string _baseUrl = "https://spp-api.scancom.net";
        private double _percent = 1;
        private Random _sloganRandom;

        public MainWindow()
        {
            try
            {
                var win2 = new Window2();
                win2.Show();

                _percent = SharedFunc.GetPercent();
                InitializeComponent();

                GetData();
                SetSloganTimer();
                SetScale();

                win2.Close();
                //InstallMeOnStartUp();

                TrayIcon();

                // Auto Proxy
                var proxy = new Settings(true);
                proxy.Show();
            }
            catch(Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            //Process.Start(e.Uri.ToString());
            if(txtHplPortalAll.Text == "View all")
            {
                txtHplPortalAll.Text = "View less";

                stpOffice.Visibility = Visibility.Hidden;
                lvOffice.Visibility = Visibility.Hidden;

                stpPlatform.Visibility = Visibility.Hidden;
                lvPlatform.Visibility = Visibility.Hidden;

                stpNews.Visibility = Visibility.Hidden;
                lvNews.Visibility = Visibility.Hidden;

                icoPortalAll.Kind = MahApps.Metro.IconPacks.PackIconMaterialKind.ArrowLeft;

                lvPortal.ItemsSource = GetPortals(string.Empty, 28);
            }
            else
            {
                txtHplPortalAll.Text = "View all";

                stpOffice.Visibility = Visibility.Visible;
                lvOffice.Visibility = Visibility.Visible;

                stpPlatform.Visibility = Visibility.Visible;
                lvPlatform.Visibility = Visibility.Visible;

                stpNews.Visibility = Visibility.Visible;
                lvNews.Visibility = Visibility.Visible;

                icoPortalAll.Kind = MahApps.Metro.IconPacks.PackIconMaterialKind.ArrowRight;

                lvPortal.ItemsSource = GetPortals(string.Empty, 4);
            }
        }

        private void Hyperlink_RequestNavigateNews(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.ToString());
        }

        private void Hyperlink_RequestNavigatePlatForm(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.ToString());
        }

        public void GetData()
        {
            lvPortal.ItemsSource = GetPortals(string.Empty, 4);
            lvNews.ItemsSource = GetNews();
            lvOffice.ItemsSource = GetMicrosoft(string.Empty);
            lvPlatform.ItemsSource = GetPlatform(string.Empty);
            lblSlogan.Text = GetSlogan();
        }

        private void SetSloganTimer()
        {
            System.Windows.Threading.DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            if(new string[] { "07:00:00", "12:00:00" }.Contains(DateTime.Now.ToString("HH:mm:ss")))
            {
                lblSlogan.Text = GetSlogan();
            }    
        }

        private List<Link> GetPortals(string searchText, int top)
        {
            var nameFilter = "";
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                nameFilter = $" and contains(Title, '{searchText}')";
            }
            var url = $"SccQuickLaunch?$top={top}&$filter=LaunchType+eq+'PORTAL'{nameFilter}&$orderBy=DisplayOrder+asc";
            var products = GetOdataResponse(url);

            return products != null && products.Count() > 0 ? products.Select(_ => new Link
            {
                LinkImage = !string.IsNullOrWhiteSpace(_.Src) ? _.Src.StartsWith("http") ? _.Src : $"{_baseUrl}/Document/download/SCC${_.Id}/{_.Src}" : string.Empty,
                Title = _.Title,
                Url = _.Link,
                Width = 90 * _percent,
                Height = 120 * _percent,
                Margin = $"{5 * _percent},0,{5 * _percent},0"
            }).ToList() : new List<Link>();
        }

        private  List<LinkNews> GetNews()
        {
            var url = "SccQuickLaunch?$top=3&$filter=LaunchType+eq+'NEWS'&$orderBy=UpdatedAt+desc";
            var products = GetOdataResponse(url);

            return products != null && products.Count() > 0 ? products.Select(_ => new LinkNews
            {
                LinkImage = !string.IsNullOrWhiteSpace(_.Src) ? _.Src.StartsWith("http") ? _.Src : $"{_baseUrl}/Document/download/SCC${_.Id}/{_.Src}" : string.Empty,
                Title = _.Title,
                Url = _.Link,
                CreatedAt = _.CreatedAt.DateTime,
                Author = _.Author,
                Note = _.Note,

                Width = 375 * _percent,
                Margin = $"{-5 * _percent},{0 * _percent},{-15 * _percent},{-3 * _percent}",
                // Margin = $"{5 * _percent},{2 * _percent},{8 * _percent},{1 * _percent}",
                ImageWidth = 100 * _percent,
                ImageHeight = 80 * _percent,
                ImageMargin = 5 * _percent,
                ContentMargin = $"0,{5 * _percent},0,{5 * _percent}",
                AuthorFontSize = 12 * _percent,
                AuthorMargin = $"0,0,0,{5 * _percent}",
                TitleFontSize = 13 * _percent,
                NoteFontSize = 11 * _percent,
                DateFontSize = 10 * _percent
            }).ToList() : new List<LinkNews>();
        }

        private List<LinkMicrosoft> GetMicrosoft(string searchText)
        {
            var nameFilter = "";
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                nameFilter = $" and contains(Title, '{searchText}')";
            }
            var url = $"SccQuickLaunch?$top=4&$filter=LaunchType+eq+'OFFICE'{nameFilter}&$orderBy=DisplayOrder+asc";
            var products = GetOdataResponse(url);

            return products != null && products.Count() > 0 ? products.Select(_ => new LinkMicrosoft
            {
                LinkImage = !string.IsNullOrWhiteSpace(_.Src) ? _.Src.StartsWith("http") ? _.Src : $"{_baseUrl}/Document/download/SCC${_.Id}/{_.Src}" : string.Empty,
                Title = _.Title,
                Note = _.Link,
                Width = 90 * _percent,
                Height = 80 * _percent,
                Margin = $"{5 * _percent},0,{5 * _percent},0"
            }).ToList() : new List<LinkMicrosoft>();
        }

        private List<LinkPOW> GetPlatform(string searchText)
        {
            var nameFilter = "";
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                nameFilter = $" and contains(Title, '{searchText}')";
            }
            var url = $"SccQuickLaunch?$top=4&$filter=LaunchType+eq+'PLATFORM'{nameFilter}&$orderBy=DisplayOrder+asc";
            var products = GetOdataResponse(url);

            return products != null && products.Count() > 0 ? products.Select(_ => new LinkPOW
            {
                LinkImage = !string.IsNullOrWhiteSpace(_.Src) ? _.Src.StartsWith("http") ? _.Src : $"{_baseUrl}/Document/download/SCC${_.Id}/{_.Src}" : string.Empty,
                Title = _.Title,
                Url = _.Link,
                Width = 90 * _percent,
                Height = 100 * _percent,
                Margin = $"{5 * _percent},0,{5 * _percent},0"
            }).ToList() : new List<LinkPOW>();
        }

        private string GetSlogan()
        {
            if(_sloganRandom == null)
            {
                _sloganRandom = new Random();
            }    
            var url = "SccQuickLaunch?$filter=LaunchType+eq+'SLOGAN'&$orderBy=DisplayOrder+asc";
            var slogans = GetOdataResponse(url);
            if(slogans != null && slogans.Count > 0)
            {
                var index = _sloganRandom.Next(slogans.Count);
                return slogans[index].Title;
            }

            return string.Empty;
        }

        private List<SccQuickLaunch> GetOdataResponse(string url)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri($"{_baseUrl}/odata/");

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            var response = client.GetStringAsync(url).Result;

            if (!string.IsNullOrWhiteSpace(response))
            {
                var odataResponse = JsonConvert.DeserializeObject<OdataResponse>(response);
                if (odataResponse != null)
                {
                    return odataResponse.Value;
                }
            }
            return null;
        }

        private async Task UpdateSloganRun(int sloganId)
        {
            HttpClient client = new HttpClient();
            var postUrl = new Uri($"{_baseUrl}/SccQuickLaunch/update-slogan-run/{sloganId}");

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            await client.PostAsync(postUrl, null);
        }

        private void StarCard_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var card = sender as StarCard;
                if (card != null && card.Tag is LinkMicrosoft)
                {
                    var appItem = card.Tag as LinkMicrosoft;
                    if (appItem != null && !string.IsNullOrWhiteSpace(appItem.Note))
                    {
                        var fileName = appItem.Note;
                        if (appItem.Note.Contains("%"))
                        {
                            var regex = new System.Text.RegularExpressions.Regex("[%][a-zA-Z]+[%]");
                            if (regex.IsMatch(appItem.Note))
                            {
                                var matches = regex.Matches(appItem.Note);
                                foreach (System.Text.RegularExpressions.Match match in matches)
                                {
                                    var environment = Environment.GetEnvironmentVariable(match.Value.Replace("%", string.Empty));
                                    if (environment != null)
                                    {
                                        fileName = fileName.Replace(match.Value, environment);
                                    }
                                }
                            }
                        }
                        Process proc = new Process();
                        proc.EnableRaisingEvents = false;
                        proc.StartInfo.FileName = fileName;
                        proc.Start();
                    }
                }
            }
            catch(Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void PackIconMaterial_MouseDown(object sender, MouseButtonEventArgs e)
        {
            txtSearch.Text = string.Empty;
            lvPortal.ItemsSource = GetPortals(string.Empty, 4);
            lvOffice.ItemsSource = GetMicrosoft(string.Empty);
            lvPlatform.ItemsSource = GetPlatform(string.Empty);
        }

        private void txtSearch_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {              
                lvPortal.ItemsSource = GetPortals(txtSearch.Text, 4);
                lvOffice.ItemsSource = GetMicrosoft(txtSearch.Text);
                lvPlatform.ItemsSource = GetPlatform(txtSearch.Text);
            }
        }

        private void StackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var panel = sender as StackPanel;
            if (panel != null && panel.Tag is string)
            {
                Process.Start(panel.Tag.ToString());
            }
        }

        public void SetScale()
        {
            if(Height != SystemParameters.WorkArea.Height)
            {
                var percent = SharedFunc.GetPercent();
                //Height *= percent;
                Height = SystemParameters.WorkArea.Height;
                Width = Width * percent + 5;

                // Position
                Left = SystemParameters.WorkArea.Width - Width;

                // slogan
                stpSlogan.Width *= percent;
                stpSlogan.Height *= percent;
                stpSlogan.Margin = new Thickness
                {
                    Left = stpSlogan.Margin.Left * percent,
                    Top = stpSlogan.Margin.Top * percent,
                    Bottom = stpSlogan.Margin.Bottom * percent
                };

                lblSlogan.FontSize *= percent;
                lblSlogan.Padding = new Thickness
                {
                    Top = lblSlogan.Padding.Top * percent,
                    Bottom = lblSlogan.Padding.Bottom * percent,
                    Right = lblSlogan.Padding.Right * percent
                };

                // search
                bdrSearch.CornerRadius = new CornerRadius
                {
                    TopLeft = bdrSearch.CornerRadius.TopLeft * percent,
                    TopRight = bdrSearch.CornerRadius.TopRight * percent,
                    BottomLeft = bdrSearch.CornerRadius.BottomLeft * percent,
                    BottomRight = bdrSearch.CornerRadius.BottomRight * percent
                };
                bdrSearch.Margin = new Thickness
                {
                    Left = bdrSearch.Margin.Left * percent,
                    Right = bdrSearch.Margin.Right * percent
                };
                bdrSearch.Height *= percent;

                icoLookup.Width *= percent;
                icoLookup.Height *= percent;
                icoLookup.FontSize *= percent;
                icoLookup.Margin = new Thickness
                {
                    Top = icoLookup.Margin.Top * percent,
                    Bottom = icoLookup.Margin.Bottom * percent
                };

                txtSearch.FontSize *= percent;
                txtSearch.Width *= percent;
                txtSearch.Height *= percent;
                txtSearch.Margin = new Thickness
                {
                    Left = txtSearch.Margin.Left * percent
                };

                icoClear.Width *= percent;
                icoClear.Height *= percent;
                icoClear.Margin = new Thickness
                {
                    Top = icoClear.Margin.Top * percent
                };

                // Logo
                imgLogo.Width *= percent;
                imgLogo.Margin = new Thickness
                {
                    Top = imgLogo.Margin.Top * percent
                };

                // Portal
                stpPortal.Height *= percent;
                stpPortalTitle.Width *= percent;
                txtPortalTitle.FontSize *= percent;
                txtPortalTitle.Margin = new Thickness
                {
                    Left = txtPortalTitle.Margin.Left * percent,
                    Top = txtPortalTitle.Margin.Top * percent
                };

                stpPortalAll.Width *= percent;
                txtPortalAll.FontSize *= percent;
                txtPortalAll.Margin = new Thickness
                {
                    Left = txtPortalAll.Margin.Left * percent,
                    Top = txtPortalAll.Margin.Top * percent,
                    Right = txtPortalAll.Margin.Right * percent
                };

                icoPortalAll.Width *= percent;
                icoPortalAll.Height *= percent;
                icoPortalAll.Margin = new Thickness
                {
                    Top = icoPortalAll.Margin.Top * percent
                };

                lvPortal.Margin = new Thickness
                {
                    Top = lvPortal.Margin.Top * percent
                };

                // Office
                stpOffice.Height *= percent;
                stpOfficeTitle.Width *= percent;
                txtOfficeTitle.FontSize *= percent;
                txtOfficeTitle.Margin = new Thickness
                {
                    Left = txtOfficeTitle.Margin.Left * percent,
                    Top = txtOfficeTitle.Margin.Top * percent
                };

                lvOffice.Margin = new Thickness
                {
                    Top = lvOffice.Margin.Top * percent
                };

                // Platform
                stpPlatform.Height *= percent;
                stpPlatformTitle.Width *= percent;
                txtPlatformTitle.FontSize *= percent;
                txtPlatformTitle.Margin = new Thickness
                {
                    Left = txtPlatformTitle.Margin.Left * percent,
                    Top = txtPlatformTitle.Margin.Top * percent
                };
                stpPlatformAll.Width *= percent;
                txtPlatformAll.FontSize *= percent;
                txtPlatformAll.Margin = new Thickness
                {
                    Left = txtPlatformAll.Margin.Left * percent,
                    Top = txtPlatformAll.Margin.Top * percent,
                    Right = txtPlatformAll.Margin.Right * percent
                };
                icoPlatformAll.Width *= percent;
                icoPlatformAll.Height *= percent;
                icoPlatformAll.Margin = new Thickness
                {
                    Top = icoPlatformAll.Margin.Top * percent
                };

                lvPlatform.Margin = new Thickness
                {
                    Top = lvPlatform.Margin.Top * percent
                };

                // News
                stpNews.Margin = new Thickness
                {
                    Left = stpNews.Margin.Left * percent,
                    Right = stpNews.Margin.Right * percent,
                    Bottom = stpNews.Margin.Bottom * percent
                };
                bdrNews.CornerRadius = new CornerRadius
                {
                    BottomLeft = bdrNews.CornerRadius.BottomLeft * percent,
                    BottomRight = bdrNews.CornerRadius.BottomRight * percent,
                    TopLeft = bdrNews.CornerRadius.TopLeft * percent,
                    TopRight = bdrNews.CornerRadius.TopRight * percent,
                };
                bdrNews.Margin = new Thickness
                {
                    Bottom = bdrNews.Margin.Bottom * percent
                };

                stpNewsHeader.Height *= percent;
                stpNewsTitle.Width *= percent;
                txtNewsTitle.FontSize *= percent;
                txtNewsTitle.Margin = new Thickness
                {
                    Left = txtNewsTitle.Margin.Left * percent,
                    Top = txtNewsTitle.Margin.Top * percent
                };

                stpNewsAll.Width *= percent;
                txtNewsAll.FontSize *= percent;
                txtNewsAll.Margin = new Thickness
                {
                    Left = txtNewsAll.Margin.Left * percent,
                    Top = txtNewsAll.Margin.Top * percent,
                    Right = txtNewsAll.Margin.Right * percent,
                    Bottom = txtNewsAll.Margin.Bottom * percent
                };
                icoNewsAll.Width *= percent;
                icoNewsAll.Height *= percent;
                icoNewsAll.Margin = new Thickness
                {
                    Top = icoNewsAll.Margin.Top * percent
                };

                // CIC
                lblCiC.Height *= percent;
                lblCiC.FontSize *= percent;
            }    
        }

        private void InstallMeOnStartUp()
        {
            try
            {
                Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                Assembly curAssembly = Assembly.GetExecutingAssembly();
                key.SetValue(curAssembly.GetName().Name, curAssembly.Location);
            }
            catch { }
        }

        private void this_Loaded(object sender, RoutedEventArgs e)
        {
            WindowStyle = WindowStyle.None;
        }

        private void TrayIcon()
        {
            var icoPath = AppDomain.CurrentDomain.BaseDirectory + "Main.ico";
            NotifyIcon ni = new NotifyIcon();
            ni.Icon = new Icon(icoPath);
            ni.Visible = true;
            ni.Text = "SCC Quick Launch";
            ni.Click +=
                delegate (object sender, EventArgs args)
                {
                    Show();
                    WindowState = WindowState.Normal;
                };
        }

        protected override void OnStateChanged(EventArgs e)
        {
            if (WindowState == WindowState.Minimized)
                this.Hide();

            base.OnStateChanged(e);
        }
    }
    public class User
    {
        public string Name { get; set; }

        public int Age { get; set; }
    }
    public class Link
    {
        public string Url { get; set; }

        public string LinkImage { get; set; }

        public string Title { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public string Margin { get; set; }
    }
    public class LinkNews
    {
        public string Url { get; set; }

        public string LinkImage { get; set; }

        public string Title { get; set; }

        public DateTime CreatedAt { get; set; }

        public string Author { get; set; }

        public string Note { get; set; }

        public double Width { get; set; }
        public string Margin { get; set; }
        public double ImageWidth { get; set; }
        public double ImageHeight { get; set; }
        public double ImageMargin { get; set; }
        public string ContentMargin { get; set; }
        public double AuthorFontSize { get; set; }
        public string AuthorMargin { get; set; }
        public double TitleFontSize { get; set; }
        public double NoteFontSize { get; set; }
        public double DateFontSize { get; set; }
    }
    public class LinkMicrosoft
    {
        public string Note { get; set; }

        public string LinkImage { get; set; }

        public string Title { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public string Margin { get; set; }
    }
    public class LinkPOW
    {
        public string Url { get; set; }

        public string LinkImage { get; set; }

        public string Title { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public string Margin { get; set; }

    }
    public class Slogan
    {      
        public string Title { get; set; }

    }
    public class Product
    {
        //public int Id { get; set; }
        public string Name { get; set; }     
    }
    public class OdataResponse
    {
        [JsonProperty("@odata.context")]
        public string Context { get; set; }

        [JsonProperty("value")]
        public List<SccQuickLaunch> Value { get; set; }
    }

    public partial class SccQuickLaunch
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public string Src { get; set; }
        public string Note { get; set; }
        public string Author { get; set; }
        public string LaunchType { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTimeOffset UpdatedAt { get; set; }
        public int UpdatedBy { get; set; }
        public short Status { get; set; }
    }
}
