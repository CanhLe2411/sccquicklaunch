﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCCQuickLaunch.Common
{
    public static class XmlClassHelper
    {
        public static void WriteXML<T>(T info, string fileName)
        {
            System.Xml.Serialization.XmlSerializer writer =
                new System.Xml.Serialization.XmlSerializer(typeof(T));
            var parentFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "//Sccql";
            if(!System.IO.Directory.Exists(parentFolder))
            {
                System.IO.Directory.CreateDirectory(parentFolder);
            }    
            var path = parentFolder + "//" + fileName;
            System.IO.FileStream file = System.IO.File.Create(path);

            writer.Serialize(file, info);
            file.Close();
        }

        public static T ReadXML<T>(string fileName)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "//Sccql//" + fileName;
            if (System.IO.File.Exists(path))
            {
                // Now we can read the serialized book ...  
                System.Xml.Serialization.XmlSerializer reader =
                    new System.Xml.Serialization.XmlSerializer(typeof(T));
                System.IO.StreamReader file = new System.IO.StreamReader(path);
                var info = (T)reader.Deserialize(file);
                file.Close();

                return info;
            }
            else
            {
                return default(T);
            }
        }
    }
}
